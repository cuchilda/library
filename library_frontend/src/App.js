import React, { Component } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Nav from './components/common/Nav';
import Home from './components/home/home';
import Signup from './components/login/Signup';
import Login from './components/login/Login';
import apiInstance from './components/api/Api';
import './App.css';
import AuthorCreator from './components/literary-generation/AuthorCreator';
import BookCreator from './components/library/BookCreator';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      displayed_form: '',
      logged_in: localStorage.getItem('token') ? true : false,
      username: ''
    };
  }

  componentDidMount() {
    if (this.state.logged_in) {
      fetch('http://localhost:8000/core/current_user/', {
        headers: {
          Authorization: `JWT ${localStorage.getItem('token')}`
        }
      })
        .then(res => res.json())
        .then(json => {
          this.setState({ username: json.username });
        });
    }
  }

  handle_login = (e, data) => {
      e.preventDefault();
      apiInstance.post('/token/obtain/', data).then(res => {
          apiInstance.defaults.headers['Authorization'] = "JWT " + res.data.access;
          localStorage.setItem('access_token', res.data.access);
          localStorage.setItem('refresh_token', res.data.refresh);
          this.setState({
            logged_in: true,
            displayed_form: '',
          });
          return res.data;
      })
      .catch(err => {})
  };

  handle_signup = (e, data) => {
    e.preventDefault();
    apiInstance.post('/user/create/', data).then(res => {
        apiInstance.defaults.headers['Authorization'] = "JWT " + res.data.access;
        localStorage.setItem('access_token', res.data.access);
        localStorage.setItem('refresh_token', res.data.refresh);
        this.setState({
          logged_in: true,
          displayed_form: '',
        });
        return res.data;
    })
    .catch(err => {})
  };

  handle_logout = () => {
    localStorage.removeItem('access_token');
    localStorage.removeItem('refresh_token');
    this.setState({ logged_in: false, username: '' });
  };

  display_form = form => {
    this.setState({
      displayed_form: form
    });
  };

  render() {
    let form;
    switch (this.state.displayed_form) {
      case 'login':
        form = <Login handle_login={this.handle_login} />;
        break;
      case 'signup':
        form = <Signup handle_signup={this.handle_signup} />;
        break;
      default:
        form = null;
    }

    return (
      <div className="App">
        <Nav
          logged_in={this.state.logged_in}
          display_form={this.display_form}
          handle_logout={this.handle_logout}
        />
        {form}
        <h3>
          {this.state.logged_in
            ? `Bienvenido a la gestión virtual de libros`
            : 'Por favor, haz login para poder hacer la gestión de los libros'}
        </h3>

        <Router>
            <div>
            <nav>
              {this.state.logged_in
                ? <nav>
                  <Link className={"nav-link"} to="/">Home</Link>
                  <Link className={"nav-link"} to="/bookcase/">Gestión de libros</Link>
                  <Link className={"nav-link"} to="/authors/">Gestión de autores</Link>
                  <button onClick={this.handleLogout}>Logout</button>
                </nav>
                : <nav>
                    <Link className={"nav-link"} to="/">Home</Link>
                  </nav>
              }
            </nav>


              <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/bookcase/" component={BookCreator}/>
                <Route exact path="/authors/" component={AuthorCreator}/>
              </Switch>

            </div>
          </Router>
        
      </div>
    );
  }
}

export default App;