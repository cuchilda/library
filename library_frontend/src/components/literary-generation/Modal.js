
    import React, { Component } from "react";
    import {
      Button,
      Modal,
      ModalHeader,
      ModalBody,
      ModalFooter,
      Form,
      FormGroup,
      Input,
      Label
    } from "reactstrap";

    export default class CustomModal extends Component {
      constructor(props) {
        super(props);
        this.state = {
          activeItem: this.props.activeItem
        };
      }
      handleChange = e => {
        let { name, value } = e.target;
        if (e.target.type === "checkbox") {
          value = e.target.checked;
        }
        const activeItem = { ...this.state.activeItem, [name]: value };
        this.setState({ activeItem });
      };
      render() {
        const { toggle, onSave } = this.props;
        return (
          <Modal isOpen={true} toggle={toggle}>
            <ModalHeader toggle={toggle}> Nuevo autor </ModalHeader>
            <ModalBody>
              <Form>
                <FormGroup>
                  <Label for="Name">Nombre</Label>
                  <Input
                    type="text"
                    name="name"
                    value={this.state.activeItem.name}
                    onChange={this.handleChange}
                    placeholder="Nombre del autor"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="surname">Apellido</Label>
                  <Input
                    type="text"
                    name="surname"
                    value={this.state.activeItem.surname}
                    onChange={this.handleChange}
                    placeholder="Apellido del autor"
                  />
                </FormGroup>
              </Form>
            </ModalBody>
            <ModalFooter>
              <Button color="success" onClick={() => onSave(this.state.activeItem)}>
                Save
              </Button>
            </ModalFooter>
          </Modal>
        );
      }
    }