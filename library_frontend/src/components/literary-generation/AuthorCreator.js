import React, { Component } from "react";
import Modal from "./Modal";
import apiInstance from "../api/Api";

class AuthorCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: {
        name: "",
        surname: "",
        completed: false
      },
      authorList: []
    };
  }
  componentDidMount() {
    this.refreshList();
  }
  refreshList = () => {
    apiInstance.get('/authors/')
    .then(res => this.setState({ authorList: res.data }) )
    .catch(err => console.log(err));
  };
  renderItems = () => {
    const newItems = this.state.authorList
    return newItems.map(item => (
      <li
        key={item.id}
        className="list-group-item d-flex justify-content-between align-items-center"
      >
        <span
          className={`todo-title mr-2`}
          title={item.name}
        >
          {item.name} {item.surname}
        </span>
        <span>
          <button
            onClick={() => this.editItem(item)}
            className="btn btn-secondary mr-2"
          >
            {" "}
            Edit{" "}
          </button>
          <button
            onClick={() => this.handleDelete(item)}
            className="btn btn-danger"
          >
            Delete{" "}
          </button>
        </span>
      </li>
    ));
  };
  toggle = () => {
    this.setState({ modal: !this.state.modal });
  };
  handleSubmit = item => {
    this.toggle();
    if (item.id) {
        apiInstance.put(`/authors/${item.id}/`, item)
        .then(res => this.refreshList());
      return;
    }
    apiInstance.post('/authors/', item)
      .then(res => this.refreshList());
  };
  handleDelete = item => {
    apiInstance.delete(`/authors/${item.id}`)
      .then(res => this.refreshList());
  };
  createItem = () => {
    const item = { name: "", surname: ""};
    this.setState({ activeItem: item, modal: !this.state.modal });
  };
  editItem = item => {
    this.setState({ activeItem: item, modal: !this.state.modal });
  };
  render() {
    return (
      <main className="content">
        <h1 className="text-white text-uppercase text-center my-4">Todo app</h1>
        <div className="row ">
          <div className="col-md-6 col-sm-10 mx-auto p-0">
            <div className="card p-3">
              <div className="">
                <button onClick={this.createItem} className="btn btn-primary">
                  Añadir autor
                </button>
              </div>
              <ul className="list-group list-group-flush">
                {this.renderItems()}
              </ul>
            </div>
          </div>
        </div>
        {this.state.modal ? (
          <Modal
            activeItem={this.state.activeItem}
            toggle={this.toggle}
            onSave={this.handleSubmit}
          />
        ) : null}
      </main>
    );
  }
}
export default AuthorCreator;