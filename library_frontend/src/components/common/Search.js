import React from 'react';
import { useState } from 'react';


function Search({allBookList}){
    const { search } = window.location;
    const query = new URLSearchParams(search).get('s');
    const [searchQuery, setSearchQuery] = useState(query || '');
    const filteredBooks = []

    const filterBooks = (books, query) => {
        if (!query) {
            return books;
        }
    
        return books.filter((post) => {
            const bookName = post.title.toLowerCase();
            return bookName.includes(query);
        });
    };

    return(
      <section className="search">
        <form action="/" method="get">
            <label htmlFor="header-search">
                <span className="visually-hidden">Buscar libros</span>
            </label>
            <input
                value={searchQuery}
                onInput={e => setSearchQuery(e.target.value)}
                type="text"
                id="header-search"
                placeholder="Buscar libros"
                name="s"
            />
            <button type="submit">Buscar</button>
        </form>
        <ul>
            {allBookList.map((book) => (
                <li key={book.id}>{book.title}</li>
            ))}
        </ul>

        <div className="grid">
            {
                allBookList.map(({ title, id}) => (
                <li
                key={id}
                className="list-group-item d-flex justify-content-between align-items-center"
                >
                <span
                    className={`todo-title mr-2`}
                    title={title}
                >
                    {title} 
                </span>
        
                </li>
                ))
            }
        </div>

    </section>
    )
  }

  export default Search