import React, { Component } from 'react'
import apiInstance from "../api/Api";

class Home extends Component {
    state = {
        allBookList : [],
        search: "",
        query: "",
      }

    componentDidMount() {
      apiInstance.get('/books/')
      .then(res => this.setState({ allBookList: res.data }) )
      .catch(err => console.log(err));
    }


    render(){
        return(
            <section className="top-cards">
            <div className="wrapper">
              <h2>Listado de libros</h2>

              <div className="grid">
          {
            this.state.allBookList.map(({ title, id}) => (
              <li
              key={id}
              className="list-group-item d-flex justify-content-between align-items-center"
            >
              <span
                className={`todo-title mr-2`}
                title={title}
              >
                {title} 
              </span>
      
            </li>
            ))
          }


              </div>
            </div>
          </section>
        )
    }

}

export default Home