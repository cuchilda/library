import React from 'react'
// import '../css/card.css';

function Card({title, literary_genre, author}){
    const genres = literary_genre.map(({name}) => `${name}`).join(', ');
    console.log(genres)
    return(
        <article className="card">
            <p className="card-title">
                {author}
            </p>
            <p className="card-followers">
                <span className="card-followers-number">{title}</span>
                <span className="card-followers-title">{genres}</span>
            </p>
            <p className="card">
                {author}
            </p>
        </article>
    )
}

export default Card