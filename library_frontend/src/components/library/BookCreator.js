import React, { Component } from "react";
import Modal from "./Modal";
import apiInstance from "../api/Api";

class BookCreator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeItem: {
        title: "",
        author: "",
        literary_genre: ""
      },
      bookList: [],
      genreList: [],
      authorList: []
    };
  }
  componentDidMount() {
    this.refreshList();
    this.refreshGenreList();
    this.refreshAuthorList();
  }
  refreshGenreList = () => {
    apiInstance.get('/literary-genres/')
    .then(res => this.setState({ genreList: res.data }) )
    .catch(err => console.log(err));
  };
  refreshAuthorList = () => {
    apiInstance.get('/authors/')
    .then(res => this.setState({ authorList: res.data }) )
    .catch(err => console.log(err));
  };
  refreshList = () => {
    apiInstance.get('/books/')
    .then(res => this.setState({ bookList: res.data }) )
    .catch(err => console.log(err));
  };
  renderItems = () => {
    const newItems = this.state.bookList
    return newItems.map(item => (
      <li
        key={item.id}
        className="list-group-item d-flex justify-content-between align-items-center"
      >
        <span
          className={`todo-title mr-2`}
          title={item.title}
        >
          {item.title} 
        </span>
        <span>
          <button
            onClick={() => this.editItem(item)}
            className="btn btn-secondary mr-2"
          >
            {" "}
            Edit{" "}
          </button>
          <button
            onClick={() => this.handleDelete(item)}
            className="btn btn-danger"
          >
            Delete{" "}
          </button>
        </span>
      </li>
    ));
  };
  toggle = () => {
    this.setState({ modal: !this.state.modal });
  };
  handleSubmit = item => {
    this.toggle();
    let data = {
      "title":item.title,
      "author": typeof item.author == "object" ? item.author.id : item.author,
    }
    if (item.id) {
        data.literary_genre = [item.literary_genre===Object ? parseInt(item.literary_genre.id) : parseInt(item.literary_genre)]
        apiInstance.put(`/books/${item.id}/`, data)
        .then(res => this.refreshList());
      return;
    }
    data.literary_genre = [item.literary_genre===Object ? parseInt(item.literary_genre.id) : parseInt(item.literary_genre)]
    apiInstance.post('/books/', data)
      .then(res => this.refreshList());
  };
  handleDelete = item => {
    apiInstance.delete(`/books/${item.id}`)
      .then(res => this.refreshList());
  };
  createItem = () => {
    const item = { title: "", literary_genre: "", author: "" };
    this.setState({ activeItem: item, modal: !this.state.modal });
  };
  editItem = item => {
    this.setState({ activeItem: item, modal: !this.state.modal });
  };
  render() {
    return (
      <main className="content">
        <h1 className="text-white text-uppercase text-center my-4">Todo app</h1>
        <div className="row ">
          <div className="col-md-6 col-sm-10 mx-auto p-0">
            <div className="card p-3">
              <div className="">
                <button onClick={this.createItem} className="btn btn-primary">
                  Añadir libro
                </button>
              </div>
              <ul className="list-group list-group-flush">
                {this.renderItems()}
              </ul>
            </div>
          </div>
        </div>
        {this.state.modal ? (
          <Modal
            activeItem={this.state.activeItem}
            toggle={this.toggle}
            onSave={this.handleSubmit}
            genreList={this.state.genreList}
            authorList={this.state.authorList}
          />
        ) : null}
      </main>
    );
  }
}
export default BookCreator;