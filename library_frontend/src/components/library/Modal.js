
    import React, { Component } from "react";
    import {
      Button,
      Modal,
      ModalHeader,
      ModalBody,
      ModalFooter,
      Form,
      FormGroup,
      Input,
      Label
    } from "reactstrap";

    export default class CustomModal extends Component {
      constructor(props) {
        super(props);
        this.state = {
          activeItem: this.props.activeItem
        };
      }
      handleChange = e => {
        let { name, value } = e.target;
        if (e.target.type === "checkbox") {
          value = e.target.checked;
        }
        if (e.target.type === "select-one") {
          value = parseInt(e.target.value);
        }
        const activeItem = { ...this.state.activeItem, [name]: value };
        this.setState({ activeItem });
      };
      render() {
        const { toggle, onSave } = this.props;
        return (
          <Modal isOpen={true} toggle={toggle}>
            <ModalHeader toggle={toggle}> Nuevo autor </ModalHeader>
            <ModalBody>
              <Form>
                <FormGroup>
                  <Label for="Name">Título</Label>
                  <Input
                    type="text"
                    name="title"
                    value={this.state.activeItem.title}
                    onChange={this.handleChange}
                    placeholder="Tñitulo de la obra"
                  />
                </FormGroup>
                <FormGroup>
                  <Label for="literary_genre">Género literario</Label>
                  <Input
                    type="select"
                    name="literary_genre"
                    value={this.state.activeItem.literary_genre}
                    onChange={this.handleChange}
                    placeholder="Género literario">
                      {this.props.genreList.map((e, key) => {
                      return <option key={key} value={e.id}>{e.name}</option>;})}
                  </Input>

                </FormGroup>
                <FormGroup>
                  <Label for="author">Autor</Label>
                  <Input
                    type="select"
                    name="author"
                    value={this.state.activeItem.author}
                    onChange={this.handleChange}
                    placeholder="Autor">
                    {this.props.authorList.map((e, key) => {
                      return <option key={key} value={e.id}>{e.name} {e.surname}</option>;})}
                  </Input>
                </FormGroup>
              </Form>
            </ModalBody>
            <ModalFooter>
              <Button color="success" onClick={() => onSave(this.state.activeItem)}>
                Save
              </Button>
            </ModalFooter>
          </Modal>
        );
      }
    }