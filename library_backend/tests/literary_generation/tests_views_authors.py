import json

from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status


class AuthorViewSetTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_superuser(
            username='Test',
            password='password',
            email='test@test.com'
        )
        self.url = '/api/authors/'
        self.url_pk = '/api/authors/{}/'

    def test_create_author(self):
        data = json.dumps({
            "name": "Author Test",
            "surname": "Surname Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.json().get('name'), 'Author Test')
        self.assertEqual(response.json().get('surname'), 'Surname Test')

    def test_error_create_duplicate_author(self):
        """Try to create twice same author"""
        data = json.dumps({
            "name": "Author Test",
            "surname": "Surname Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response_duplicated = client.post(self.url, data=data,
                                         content_type='application/json')
        self.assertEqual(response_duplicated.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_duplicated.json(), {
            "non_field_errors": ["The fields name, surname must make a unique set."]})


    def test_create_error_author(self):
        """ When create author, not send surname field (required)"""

        data = json.dumps({
            "name": "Author Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {"surname": ["This field is required."]})

    def test_remove_author(self):
        data = json.dumps({
            "name": "Author Test",
            "surname": "Surname Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response_create = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response_create.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_create.json().get('name'), 'Author Test')
        self.assertEqual(response_create.json().get('surname'), 'Surname Test')

        author_id = response_create.json().get('id')
        response_delete = client.delete(self.url_pk.format(author_id),
                                        content_type='application/json')

        self.assertEqual(response_delete.status_code, status.HTTP_204_NO_CONTENT)

    def test_update_author(self):
        data = json.dumps({
            "name": "Author Test",
            "surname": "Surname Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response_create = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response_create.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_create.json().get('name'), 'Author Test')
        self.assertEqual(response_create.json().get('surname'), 'Surname Test')

        data_update = json.dumps({
            "name": "Author Test New",
            "surname": "Surname Test"
        })
        author_id = response_create.json().get('id')
        response_update = client.put(
            self.url_pk.format(author_id), content_type='application/json',
            data=data_update
        )

        self.assertEqual(response_update.status_code, status.HTTP_200_OK)
        self.assertEqual(response_update.json().get('name'), 'Author Test New')
        self.assertEqual(response_update.json().get('surname'), 'Surname Test')

    def test_error_update_author(self):
        """ When update author, not send surname field (required)"""
        data = json.dumps({
            "name": "Author Test",
            "surname": "Surname Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response_create = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response_create.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_create.json().get('name'), 'Author Test')
        self.assertEqual(response_create.json().get('surname'), 'Surname Test')

        data_update = json.dumps({
            "name": "Author Test New",
        })
        author_id = response_create.json().get('id')
        response_update = client.put(
            self.url_pk.format(author_id), content_type='application/json',
            data=data_update
        )

        self.assertEqual(response_update.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_update.json(), {"surname": ["This field is required."]})

    def test_list_authors(self):
        data_1 = {
            "name": "Author Test One",
            "surname": "Surname Test One",
        }
        data_2 = {
            "name": "Author Test Two",
            "surname": "Surname Test Two",
        }
        data_3 = {
            "name": "Author Test Three",
            "surname": "Surname Test Three",
        }
        client = APIClient()
        client.force_authenticate(user=self.user)

        response_create_1 = client.post(self.url, data=json.dumps(data_1),
                                       content_type='application/json')
        self.assertEqual(response_create_1.status_code, status.HTTP_201_CREATED)
        author_id_1 = response_create_1.json().get('id')
        data_1.update({"id": author_id_1})
        response_create_2 = client.post(self.url, data=json.dumps(data_2),
                                       content_type='application/json')
        self.assertEqual(response_create_2.status_code, status.HTTP_201_CREATED)
        author_id_2 = response_create_2.json().get('id')
        data_2.update({"id": author_id_2})
        response_create_3 = client.post(self.url, data=json.dumps(data_3),
                                       content_type='application/json')
        self.assertEqual(response_create_3.status_code, status.HTTP_201_CREATED)
        author_id_3 = response_create_3.json().get('id')
        data_3.update({"id": author_id_3})

        response_list = client.get(self.url, data=data_1,
                                       content_type='application/json')

        self.assertEqual(response_list.status_code, status.HTTP_200_OK)
        self.assertEqual(response_list.json(), [data_1, data_2, data_3])