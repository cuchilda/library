import json

from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status


class AuthorViewSetTest(TestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_superuser(
            username='Test',
            password='password',
            email='test@test.com'
        )
        self.url = '/api/literary-genres/'
        self.url_pk = '/api/literary-genres/{}/'

    def test_create_literary_genre(self):
        data = json.dumps({
            "name": "Genre one",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.json().get('name'), 'Genre one')

    def test_error_create_duplicate_literary_genre(self):
        """Try to create twice same literary_genre"""
        data = json.dumps({
            "name": "Genre Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response_duplicated = client.post(self.url, data=data,
                                         content_type='application/json')
        self.assertEqual(response_duplicated.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_duplicated.json(), {
            "non_field_errors": ["The fields name must make a unique set."]})


    def test_create_error_literary_genre(self):
        """ When create literary_genre, not send surname field (required)"""

        data = json.dumps({
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.json(), {"name": ["This field is required."]})

    def test_remove_literary_genre(self):
        data = json.dumps({
            "name": "Genre Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response_create = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response_create.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_create.json().get('name'), 'Genre Test')

        genre_id = response_create.json().get('id')
        response_delete = client.delete(self.url_pk.format(genre_id),
                                        content_type='application/json')

        self.assertEqual(response_delete.status_code, status.HTTP_204_NO_CONTENT)

    def test_update_literary_genre(self):
        data = json.dumps({
            "name": "Genre Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response_create = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response_create.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_create.json().get('name'), 'Genre Test')

        data_update = json.dumps({
            "name": "Genre Test Update",
        })
        author_id = response_create.json().get('id')
        response_update = client.put(
            self.url_pk.format(author_id), content_type='application/json',
            data=data_update
        )

        self.assertEqual(response_update.status_code, status.HTTP_200_OK)
        self.assertEqual(response_update.json().get('name'), 'Genre Test Update')

    def test_error_update_literary_genre(self):
        """ When update author, not send surname field (required)"""
        data = json.dumps({
            "name": "Genre Test",
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response_create = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response_create.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response_create.json().get('name'), 'Genre Test')

        data_update = json.dumps({
        })
        author_id = response_create.json().get('id')
        response_update = client.put(
            self.url_pk.format(author_id), content_type='application/json',
            data=data_update
        )

        self.assertEqual(response_update.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response_update.json(), {"name": ["This field is required."]})

    def test_list_literary_genres(self):
        data_1 = {
            "name": "Genre Test One",
        }
        data_2 = {
            "name": "Genre Test Two",
        }
        data_3 = {
            "name": "Genre Test Three",
        }
        client = APIClient()
        client.force_authenticate(user=self.user)

        response_create_1 = client.post(self.url, data=json.dumps(data_1),
                                       content_type='application/json')
        self.assertEqual(response_create_1.status_code, status.HTTP_201_CREATED)
        literary_genre_id_1 = response_create_1.json().get('id')
        data_1.update({"id": literary_genre_id_1})
        response_create_2 = client.post(self.url, data=json.dumps(data_2),
                                       content_type='application/json')
        self.assertEqual(response_create_2.status_code, status.HTTP_201_CREATED)
        literary_genre_id_2 = response_create_2.json().get('id')
        data_2.update({"id": literary_genre_id_2})
        response_create_3 = client.post(self.url, data=json.dumps(data_3),
                                       content_type='application/json')
        self.assertEqual(response_create_3.status_code, status.HTTP_201_CREATED)
        literary_genre_id_3 = response_create_3.json().get('id')
        data_3.update({"id": literary_genre_id_3})

        response_list = client.get(self.url, data=data_1,
                                       content_type='application/json')

        self.assertEqual(response_list.status_code, status.HTTP_200_OK)
        self.assertEqual(response_list.json(), [data_1, data_2, data_3])