import json

from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status

from tests.constants import FIXTURE_GENRES_DIR, FIXTURE_AUTHORS_DIR
from bookcase.models import LiteraryGenre
from literary_generation.models import Author


class BookViewSetTest(TestCase):

    fixtures = [FIXTURE_GENRES_DIR, FIXTURE_AUTHORS_DIR]
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_superuser(
            username='Test',
            password='password',
            email='test@test.com'
        )
        self.url = '/api/literary-genres/'
        self.url_pk = '/api/literary-genres/{}/'

    def test_create_book(self):
        author = Author(name="Author", surname="Test")
        author.save()

        data = json.dumps({
            "name": "Book one",
            "author": 1,
            "literary_genre": 4
        })
        client = APIClient()
        client.force_authenticate(user=self.user)
        response = client.post(self.url, data=data, content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.json().get('name'), 'Book one')
