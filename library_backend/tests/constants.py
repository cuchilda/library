import os


BASE_TEST_DIR = os.path.dirname(os.path.abspath(__file__))
FIXTURE_AUTHORS_DIR = os.path.join(BASE_TEST_DIR, 'fixtures', 'authors.json')
FIXTURE_GENRES_DIR = os.path.join(BASE_TEST_DIR, 'fixtures', 'literary_genres.json')
print(FIXTURE_GENRES_DIR)