from .views import AuthorViewSet

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'authors', AuthorViewSet, basename='authors-list')
urlpatterns = router.urls
