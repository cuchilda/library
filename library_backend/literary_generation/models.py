from django.db import models
from django.utils.translation import ugettext_lazy as _


class Author(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)
    surname = models.CharField(max_length=255, null=False, blank=False)

    class Meta:
        unique_together = ('name', 'surname',)
        verbose_name = _('Author')
        verbose_name_plural = _('Authors')

    def __str__(self):
        return "{} {}".format(self.name, self.surname)