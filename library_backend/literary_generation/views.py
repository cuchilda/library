from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from .models import Author
from .serializers import AuthorSerializer


class AuthorViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Author.objects.order_by("name")
    serializer_class = AuthorSerializer

    def list(self, request):
        queryset = Author.objects.all()
        serializer = AuthorSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Author.objects.all()
        book = get_object_or_404(queryset, pk=pk)
        serializer = AuthorSerializer(book)
        return Response(serializer.data)

author_list = AuthorViewSet.as_view({'get': 'list'})
author_detail = AuthorViewSet.as_view({'get': 'retrieve'})
