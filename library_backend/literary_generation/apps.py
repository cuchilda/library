from django.apps import AppConfig


class LiteraryGenerationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'literary_generation'
