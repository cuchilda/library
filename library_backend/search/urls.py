from django.urls import path

from search.views import SearchAuthors, SearchBooks

urlpatterns = [
    path('author/<str:query>/', SearchAuthors.as_view()),
    path('book/<str:query>/', SearchBooks.as_view()),
]