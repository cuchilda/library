from rest_framework import serializers
from .models import Book, LiteraryGenre
from literary_generation.serializers import AuthorSerializer

class LiteraryGenreSerializer(serializers.ModelSerializer):
    class Meta:
        model = LiteraryGenre
        fields = '__all__'


class BookSerializer(serializers.ModelSerializer):
    literary_genre = LiteraryGenreSerializer(many=True)
    author = AuthorSerializer()

    class Meta:
        model = Book
        fields = '__all__'


class BookCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        fields = '__all__'
