from .views import BookViewSet, LiteraryGenresViewSet

from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'books', BookViewSet, basename='books-list')
router.register(r'literary-genres', LiteraryGenresViewSet, basename='literary-genres-list')
urlpatterns = router.urls

