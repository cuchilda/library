from django.db import models
from django.utils.translation import ugettext_lazy as _
from literary_generation.models import Author


class LiteraryGenre(models.Model):
    name = models.CharField(max_length=255, null=False, blank=False)

    class Meta:
        unique_together = ('name',)
        verbose_name = _('Literary genre')
        verbose_name_plural = _('Literary genres')

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=255, null=False, blank=False)
    literary_genre = models.ManyToManyField(LiteraryGenre, blank=True)
    author = models.ForeignKey(Author, null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        unique_together = ('title', )
        verbose_name = _('Book')
        verbose_name_plural = _('Books')

    def __str__(self):
        return self.title
