from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework import status

from .models import Book, LiteraryGenre
from .serializers import BookSerializer, BookCreateSerializer, LiteraryGenreSerializer


class BookViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Book.objects.order_by("title")
    serializer_class = BookSerializer

    def create(self, request, *args, **kwargs):
        serializer = BookCreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = BookCreateSerializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    def list(self, request):
        queryset = Book.objects.all()
        serializer = BookSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Book.objects.all()
        book = get_object_or_404(queryset, pk=pk)
        serializer = BookSerializer(book)
        return Response(serializer.data)

book_list = BookViewSet.as_view({'get': 'list'})
book_detail = BookViewSet.as_view({'get': 'retrieve'})


class LiteraryGenresViewSet(viewsets.ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = LiteraryGenre.objects.order_by("name")
    serializer_class = LiteraryGenreSerializer

    def list(self, request):
        queryset = LiteraryGenre.objects.all()
        serializer = LiteraryGenreSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = LiteraryGenre.objects.all()
        book = get_object_or_404(queryset, pk=pk)
        serializer = LiteraryGenreSerializer(book)
        return Response(serializer.data)

author_list = LiteraryGenresViewSet.as_view({'get': 'list'})
author_detail = LiteraryGenresViewSet.as_view({'get': 'retrieve'})
