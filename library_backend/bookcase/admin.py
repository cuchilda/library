from django.contrib import admin
from .models import Book, LiteraryGenre


@admin.register(Book)
class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'author',)
    search_fields = ('title',)
    list_filter = ('literary_genre',)


@admin.register(LiteraryGenre)
class LiteraryGenreAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ('name',)
