# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary


## Quick summary ##
To start the project you have to execute the following commands:
docker-compose build
docker-compose up

It is necessary to execute the database migrations to create the necessary models. To do this, we need to know the name of the docker corresponding to the back, we list the containers with: docker ps.
And once the back container is located, we execute:

docker exec -it library_backend-django_1 python manage.py migrate

To populate the table of literary genres and authors, we run:
docker exec -it library_backend-django1 python manage.py loaddata tests/fixtures/authors.json
docker exec -it library_backend-django1 python manage.py loaddata tests/fixtures/literary_genres.json

In order to access the backend admin we need to create a user admin:
docker exec -it library_backend-django1 python manage.py createsuperuser

And now we can access to the backend, with the url: https://localhost:8000/admin and to the frontend with the url: https://localhost:3000

In the frontend, we can click on Sign up, to create a user and log in automatically.
