# Frontend
FROM node:10.15 AS npm-builder
WORKDIR /app
COPY ./library_frontend ./
ADD ./library_frontend/package.json /package.json
RUN npm install -S --save
RUN npm install axios --save
EXPOSE 3000
CMD [ "npm", "start" ]

# Backend
FROM python:3.8 as active-server
ENV PYTHONUNBUFFERED=1
WORKDIR /app
COPY ./library_backend/requirements.txt /app
RUN pip install -r requirements.txt
COPY library_backend/ /app

